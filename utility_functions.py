import ase.io.vasp as vp
import numpy as np
from ase.build import mx2
import csv
import pandas as pd
from pymatgen.core.composition import Composition
from pymatgen.symmetry.groups import SpaceGroup
from pymatgen import MPRester, Element
from pymatgen.io.cif import CifParser
import itertools
import logging


def check_convergence(dir_name, file_name, boron=True, data_dir='data'):
    ''' Checks convergence file the vasp output file.
    
    Constructs file path from the given input parameters
    and checks the last line to check if the calculation
    is converged or not.
    '''
    
    if boron:
        elem_str='boron'
    else:
        elem_str='nitrogen'
    
    full_path = path.join(dir_name, elem_str,data_dir, file_name)
    
    with open(full_path, 'r') as fh:
        
        for line in fh:
            pass
        
        last = line
        
    first_word = last.split()[0]
    
    if first_word == 'reached':
        return True
    else:
        return False
        
def calc_binding_energy(sup_size, sites, boron = True):
    ''' Calculates binding energy of a B/N atom on 111 surface.
    
    This function is used to calculate binding energy of a
    B/N atom on a 111 supercell surface. The function
    reads the energies from a text file inside a directory
    structure and returns
    binding energy of the B/N atom normalized by the
    supercell size.
    
    The code assumes the directory structure as:
    supercell size -> boron/nitrogen -> data file
    
    The structure of text file with the energies are:
    
    Config     Energy (eV)
     hcp       -198.4350
     ni-surf   -192.3473
     fcc       -197.9562
     ontop     -196.5833

    
    '''

    all_energies = {}
    
    if boron:
        elem_str='boron'
        E_bn = -8.3205
    else:
        elem_str='nitrogen'
        E_bn = -6.703

    for i in sup_size:

        if i != 'prist':
            dir_name = '{}x{}'.format(i,i)
        else:
            dir_name = i


        file_path = path.join(dir_name,elem_str, 'bn_summary.txt')

        with open(file_path, 'r') as f:

            this_dict = {}

            for line in f.readlines():

                values = line.split()

                if values[0] == 'Config':
                    pass;
                else:
                    this_dict[values[0]] = float(values[1])
        
        if not boron:
            file_path = path.join(dir_name,'boron', 'bn_summary.txt')

            with open(file_path, 'r') as f:

                for line in f.readlines():

                    values = line.split()

                    if values[0] == 'Config':
                        pass;
                    elif values[0] == 'ni-surf':
                        this_dict[values[0]] = float(values[1])

        all_energies[i] = this_dict


    for sup in sup_size:
        
        
        if sup != 'prist':
            dir_name = '{}x{}'.format(sup,sup)
        else:
            dir_name = sup

        E_co = all_energies[sup]['ni-surf']

        for site in sites:
            
            vasp_out_file_name = '{}.vasp.out'.format(site)
            
            if check_convergence(dir_name, vasp_out_file_name):

                E_site = all_energies[sup][site]

                if sup == 'prist':
                    Eb = (E_site - E_bn - E_co)
                else:
                    Eb = (E_site - E_bn - E_co)

                all_energies[sup][site]=Eb

                print(' Binding energy of {} in {} for site {} is {:.3}eV'.format(elem_str, dir_name,site,Eb))
                
            else:
                print(' Site {} for supercell {} of {} is not converged.'.format(site,dir_name,elem_str))
                

        print('')
        
    return all_energies
    
def get_distance(sup_size, sites, boron = True, sub_elem='Ni'):
    ''' Calculates distance of adsorption site from a surface.
    
    This function can be used to calculate distance of an
    adsorption atom/layer from a surface. The function
    reads the structure from a directory structure
    and returns the distance.
    
    The returned value is a dictionary with the sites as key
    '''
    
    all_distance = {}
    
    if boron:
        elem_str='boron'
    else:
        elem_str='nitrogen'

    for i in sup_size:

        if i != 'prist':
            dir_name = '{}x{}'.format(i,i)  
            print(' Supercell size ({}): {}x{}x4\n'.format(elem_str,i,i))
        else:
            dir_name=i
            print(' Supercell {}: \n'.format(i))

        dir_path = path.join(dir_name,elem_str,'data');

        this_dict = {}

        for site in sites:
            poscar_name = '{}.CONTCAR.vasp'.format(site)

            poscar_path = path.join(dir_path, poscar_name)

            this_struc = vp.read_vasp(poscar_path)

            co_z = [a.z for a in this_struc if a.symbol==sub_elem]

            bn_z = [a.z for a in this_struc if a.symbol=='B' or a.symbol=='N']

            dist = min(bn_z) - max(co_z)

            this_dict[site] = dist

            print('\t Distance for B {}: {:0.4} A'.format(site, dist))

        print('')

        all_distance[i] = this_dict
        
    return all_distance

def unit_vector(vector):
    """ Returns the unit vector of the vector.  """
    return vector / np.linalg.norm(vector)

def angle_between(v1, v2):
    """ Returns the angle in radians between vectors 'v1' and 'v2'::

            >>> angle_between((1, 0, 0), (0, 1, 0))
            1.5707963267948966
            >>> angle_between((1, 0, 0), (1, 0, 0))
            0.0
            >>> angle_between((1, 0, 0), (-1, 0, 0))
            3.141592653589793
    """
    v1_u = unit_vector(v1)
    v2_u = unit_vector(v2)
    return np.arccos(np.clip(np.dot(v1_u, v2_u), -1.0, 1.0))

def rotate_towards(v1, v2, theta):
    ''' Rotates one vector v1 towards v2 by angle theta
    
    From: https://stackoverflow.com/a/22101541
    
    '''
    
    Dprime = unit_vector(np.cross(np.cross(v1,v2),v1))
    
    Z = np.cos(theta)*v1 + np.sin(theta)*Dprime
    
    return Z

def bulk_to_nlayer(bulk, layer=3, vac=10.0):
    ''' Returns a n-layer surface given a 2H bulk 2D material
    
    '''
    
    surf = bulk.copy()

    mult = np.ceil(layer/2)

    z=(surf.get_cell()[2,2])/2*layer

    surf= surf * (1,1,int(mult))

    del surf[[atom.index for atom in surf if atom.position[2]>= z]]

    surf.center(vacuum=vac, axis=2)
    
    return surf


def Mn_initial_moment(mnp,mag_mom=3,def_mom=1):
    """ This function initialized magnetic moments for MnBX3 AFM materials.
    MnBX3 are Neel type AFM materials. In the same layer the ordering are alternate.
    And the two Mn atoms of two layer in same position have alternating ordering.
    The def_mom is the default magnetic moment for atoms other than Mn.
    The mag_mom is the magnetic moment for Mn atoms.
    If mag_mom have a single value, then the values are assigned for collinear calculations.
    If mag_mom is a list of three values, then the values are assigned for non-collinear calculations."""
    
    if isinstance(mag_mom, list):
        list_flag=True
    else:
        list_flag=False

    z_height = mnp.get_cell()[2,2]

    ind_temp=0
    pos_temp=[]
    mom_temp=[]
    pos_temp2=[]
    mom_temp2=[]

    mnp2 = sort(mnp, tags=mnp.get_positions()[:,2])

    for idx1,atom in enumerate(mnp2):

        atom.magmom= def_mom

        if atom.symbol == 'Mn' and atom.position[2]< z_height/3 :

            if ind_temp == 0:
                atom.magmom=mag_mom
                pos_temp.append(atom.position[0:2])
                mom_temp.append(atom.magmom)
                ind_temp=1

            elif ind_temp == 1:
                if not list_flag:
                    atom.magmom=-mag_mom
                else:
                    atom.magmom=[-i for i in mag_mom]
                pos_temp.append(atom.position[0:2])
                mom_temp.append(atom.magmom)            


        elif atom.symbol == 'Mn' and atom.position[2]< z_height/3*2 and atom.position[2]> z_height/3:


            for idx,i in enumerate(pos_temp):  

                if (abs(atom.position[0:2]-i).max()) < 1e-03:
                    if not list_flag:
                        atom.magmom = -mom_temp[idx]
                    else:
                        atom.magmom = [-i for i in mom_temp[idx]]
                    mom2 = atom.magmom
                    pos_temp2.append(atom.position[0:2])
                    mom_temp2.append(atom.magmom) 
                    ind21=idx1

    for idx1,atom in enumerate(mnp2):

        if atom.symbol == 'Mn' and atom.position[2]< z_height/3*2 and atom.position[2]> z_height/3 and idx1 != ind21:
            
            if not list_flag:
                atom.magmom = -mom2
            else:
                atom.magmom = [-i for i in mom2]
                
            pos_temp2.append(atom.position[0:2])
            mom_temp2.append(atom.magmom) 

        elif atom.symbol == 'Mn' and atom.position[2]< z_height and atom.position[2]> z_height/3*2:

            for idx,i in enumerate(pos_temp2):

                if (abs(atom.position[0:2]-i).max()) < 1e-03 :
                    if not list_flag:
                        atom.magmom = -mom_temp2[idx]  
                    else:
                        atom.magmom = [-i for i in mom_temp2[idx]]
                    ind31=idx1
                    mom3= atom.magmom

    for idx1,atom in enumerate(mnp2):

        if atom.symbol == 'Mn' and atom.position[2]< z_height and atom.position[2]> z_height/3*2 and idx1 != ind31:
            
            if not list_flag:
                atom.magmom = -mom3
            else:
                atom.magmom = [-i for i in mom3]
                
    return mnp2


def htot(struc, vac=7.5):
    '''Returns a one layer surface from 2H bulk 2D material
    
    '''
    
    pos = struc.get_positions()
    
    half_z = ((max(pos[:,2])-min(pos[:,2]))/2)+min(pos[:,2])
    
    surf=struc.copy()
    
    # delete atoms these are more than half way from below
    del surf[[atom.index for atom in surf if atom.position[2]>=half_z]]
    
    # add vacuum
    surf.center(vacuum=vac, axis=2)
    
    return surf
    
def lattice_mismatch(struc1, struc2, show=False):
    ''' Returns lattice mismatch between two structures.
    '''

    vec1 = pmg.get_structure(struc1).lattice.abc[0]
    vec2 = pmg.get_structure(struc2).lattice.abc[0]
    
    form1 = struc1.get_chemical_formula()
    form2 = struc2.get_chemical_formula()

    lat_mismatch = abs((vec1-vec2)/vec1*100)
    
    if show:
        print('Lattice constant of {} is {:2.2f} angstrom.'.format(form1,vec1))

        print('Lattice constant of {} is {:2.2f} angstrom'.format(form2,vec2))

        print('Lattice mismatch is %2.2f%%'%lat_mismatch)
        
    return vec1, vec2, lat_mismatch  

def poscar_2hto1t(struc_str, vac=7.5):
    ''' Writes POSCAR of 1 layer 2H surface.
    
    Given a bulk 2H material, it calls htot function
    which returns a 1layer 2H surface. Finally the 
    structure is written in vasp5 POSCAR format.
    '''
    
    struc = vp.read_vasp(struc_str)
    
    surf = htot(struc, vac=vac)
    
    str1=struc_str.rsplit('_')[0]
    
    str2='{}_1T.vasp'.format(str1)
    
    str3='{} 1T surface'.format(str1)
    
    vp.write_vasp(str2, surf, label=str3,direct=False,sort=True, vasp5=True)
    
def aa_ab(b, t, stacking='AA', vac=7.5):
    ''' Creates a heterostructure from the given materials.
    
    Given the bottom and top structure,
    the function creates a heterostructure surface.
    The default stacking is AA.
    Other stacking possible are AB and AC.
    Stacking is based on the position of 
    metal atom.
    Default vacuum for the surface is 7.5*2=15 angstrom
    '''

    metal = ['Ta','Mo','W']
    chalcogen=['S','Se','Te']
    
    bottom = b.copy()
    top = t.copy()
    
    b_pos=bottom.get_positions()

    b_cell = bottom.get_cell()
    t_cell = top.get_cell()

    b_cell[:,2]=t_cell[:,2] 				# only take a and b
    top.set_cell(b_cell, scale_atoms=True) 	# strain top to bottom
    top.wrap()    
    height = b_pos[:,2].max()    
    het=bottom.copy()

    if stacking == 'AA':

        for atom1 in bottom:

            if atom1.symbol in metal:
                met_b_pos = atom1.position
                break

        for atom2 in top:

            if atom2.symbol in metal:
                met_t_pos = atom2.position 
                break
                
    elif stacking == 'AB':
        
        top.rotate('z', np.pi, center=(0, 0, 0))
        top.wrap()

        for atom1 in bottom:

            if atom1.symbol in metal:
                met_b_pos = atom1.position
                break

        for atom2 in top:

            if atom2.symbol in chalcogen:
                met_t_pos = atom2.position 
                break
                
    elif stacking == 'AC': 
        
        top.rotate('z', np.pi, center=(0, 0, 0))
        top.wrap()

        for atom1 in bottom:

            if atom1.symbol in metal:
                met_b_pos = atom1.position
                break

        for atom2 in top:

            if atom2.symbol in metal:
                met_t_pos = atom2.position  
                break
		
    else:
        raise NotImplementedError('{} stacking not implemented.'.format(stacking))



    shift=met_b_pos[:2]-met_t_pos[:2]  # final - initial
	
    pos_t = top.get_positions()


    pos_t[:,0] = pos_t[:,0] + shift[0]
    pos_t[:,1] = pos_t[:,1] + shift[1]

    t_shifted = top.copy()

    t_shifted.set_positions(pos_t)

    t_shifted.wrap()

    t_shifted.center(vacuum=height + 3, axis=2)

    het.extend(t_shifted)

    het.center(vacuum=vac, axis=2)
	
    del top
    
    return het

def make2h(formula,a=3.18,thick=3.19, vac=1.5):
    ''' Creates a bulk 2H structure from the input parameters.
    
    Formula is like MX2, a is the lattice constance,
    thick is the thickness of the layer and vac is
    the vacuum on each side of the layer.
    '''
    
    down=mx2(formula=formula, kind='2H', a=a, thickness=thick, size=(1, 1, 1), vacuum=vac)
    
    down_pos=down.get_positions()
    
    down_pos[:,0]=down_pos[:,0]+0.1
    
    down_pos[:,1]=down_pos[:,1]+0.1
    
    down.set_positions(down_pos)
    
    for idx,a in enumerate(down):
        
        if idx==1:
            met_b_pos = a.position
    
    up = down.copy()

    up.rotate('z',a=np.pi,center=(0,0,0))
    up.wrap()
    
    for idx,b in enumerate(up):
        
        if idx==0:
            met_t_pos = b.position    
    
    shift=met_b_pos[:2]-met_t_pos[:2]  # final - initial
    
    pos_t = up.get_positions()

    pos_t[:,0] = pos_t[:,0] + shift[0]
    pos_t[:,1] = pos_t[:,1] + shift[1]

    t_shifted = up.copy()

    t_shifted.set_positions(pos_t)

    t_shifted.wrap()    
    
    p = t_shifted.get_positions()

    down.center(vacuum=p[:, 2].max() + 3, axis=2)

    mx2_2h = t_shifted.copy()

    mx2_2h.extend(down)

    mx2_2h.center(vacuum=vac, axis=2)
    
    return mx2_2h

	
def create_csv(filename):
    """Function to create csv file.
    
    This function creates a csv file with the column
    names mentioned in the fieldnames variable."""

    fieldnames = [ 'material', 'mp_id', 'mw_id','icsd_id', 'mat_1_symbol', 'mat_1_z', 'mat_1_frac',
                  'mat_2_symbol', 'mat_2_z', 'mat_2_frac',
                  'n_sites', 'a', 'b', 'c', 'alpha', 'beta', 'gamma',
                  'bandgap', 'gs_energy', 'sg_no', 'e_hull_atom', 'volume',
                  'formation_e', 'density', '2d']
    with open(filename, 'w', newline='') as csvfile:
        
        writer = csv.DictWriter(csvfile, fieldnames=fieldnames)
        writer.writeheader()
        dwriter = csv.writer(csvfile, dialect='excel', delimiter=',')

def append_csv(filename, row_list):
    """Append to the created CSV file.
    
    Open CSV file in append mode and
    append row to the end.
    """
    with open(filename, 'a', newline='') as csvfile:
        dwriter = csv.writer(csvfile)
        dwriter.writerow(row_list)
		
def composition_list(start_z, end_z, noatoms, radioactive=['Tc','Pm']):
    """Function to create list of compositions
    
    This function creates a list of composition from atomic number
	`start_z` to `end_z`. Then returns a permutation of composition
    as a list.
	
	Optionally avoids elements supplied in radioactive list. Default
	elements in the radioactive list are Tc and Pm."""
    
    symbols = [ Element.from_Z(z).symbol for z in range(start_z, end_z+1) if Element.from_Z(z).symbol not in radioactive]
    
    # make a iterable of all binaries
    compositions = itertools.combinations(symbols, noatoms)
    
    return list(compositions)
	
def match_2d(pretty_mat_name, mat_sg, df_2d):
    """This function matches whether the material is a layered 2D material or not
    
    This function takes pretty formula, spacegroup and 2D dataframe
    as input and returns 1 if material is 2D and 0 otherwise.
    """
    
    mw_id_dummy = 'None'
    
    selected = df_2d.loc[(df_2d['Material_Name'] == pretty_mat_name) & (df_2d['spacegroup_no']==mat_sg)]
    
    if len(selected.index)>1:
        raise('More than one value in function match_2d.')
    else:
        if selected.empty:
            return (0,mw_id_dummy)
        else:
            print(selected['Material_ID'])
            return (1,mw_id_dummy)

    

def get_data(composition, df_2d):
	""" Gets data from Materials Project using the MPRester API.
	
	Creates a data row from the given composition using the MPRester API
	for a binary composition. Can be extended to other systems too.
	Returns a row which can be appended using the append_csv function.
	"""
	
    global API_KEY
    global noatoms
    
    row_lists=[]
    
    m = MPRester(API_KEY)

    if noatoms == 2:
        results = m.get_data(composition[0] + '-' + composition[1],
                             data_type='vasp')  # Download DFT data for each binary system
    elif noatoms > 2:
        raise ValueError('Not coded for number of atoms greater than 2.')

    if results:  # if not empty then do the rest. or go for the next material.

        for idx,mat in enumerate(results):

            pretty_mat = mat['pretty_formula']

            mat_sg = mat['spacegroup']
            mat_sg_no = mat_sg['number']
            mp_id_temp = mat['material_id']
            form_energy_temp = mat['formation_energy_per_atom']
            density_temp = mat['density']
            e_hull_temp = mat['e_above_hull']
            nsites_temp = mat['nsites']
            bg_temp = mat['band_gap']
            energy_temp = mat['energy']
            vol_temp = mat['volume']
            icsd_id = mat['icsd_id']
            
            if icsd_id is None:
                icsd_id = str(None)

            comp = Composition(pretty_mat)

            mat_frac = []  # atomic fraction of the system
            mat_z = []  # atomic number of the system
            mat_sym = []  # symbols of the system

            z1 = comp.elements[0].Z
            z2 = comp.elements[1].Z

            for elem in comp:
                if z2 > z1:
                    mat_frac.append(comp.get_atomic_fraction(elem))
                    mat_z.append(str(Element(elem).Z))
                    mat_sym.append(Element(elem).symbol)
                elif z1 > z2:
                    mat_frac.appendleft(comp.get_atomic_fraction(elem))
                    mat_z.appendleft(str(Element(elem).Z))
                    mat_sym.appendleft(Element(elem).symbol)
                else:
                    raise ValueError('Atomic numbers cannot be same.')
            
            flag_2d = match_2d(pretty_mat, mat_sg_no, df_2d)

            # collecting abc and alpha, beta and gamma
            cif_str = str(mat['cif'])

            with open(tempcif, 'w', newline='') as cif:
                cif.write(cif_str)

            parser = CifParser(tempcif)

            structure = parser.get_structures()[0]

            abc = structure.lattice.abc
            angles = structure.lattice.angles

            row_list = ([pretty_mat] + [mp_id_temp] + [mw_id_temp] + [icsd_id] +
                  [mat_sym[0]] + [mat_z[0]] + [mat_frac[0]] +
                  [mat_sym[1]] + [mat_z[1]] + [mat_frac[1]] +
                  [nsites_temp] + [abc[0]] + [abc[1]] + [abc[2]] +
                  [angles[0]] + [angles[1]] + [angles[2]] +
                  [bg_temp] + [energy_temp] + [mat_sg_no] + [e_hull_temp] +
                  [vol_temp] + [form_energy_temp] + [density_temp] + [flag_2d])

            row_lists.append(row_list)
            
            logging.info(' Material {}-{} done.'.format(pretty_mat, mat_sg_no))
    else:
        logging.info(' Material {} is non-existent in MP.'.format(composition))
    return row_lists
	
def do_all(db_2d_file, final_csv, start_z, end_z, noatoms):
    """Combines all the function for data collection.
    
    Opens 2D database and put that into a dataframe.
    Then creates a list of composition based on range of atomic
    number and number of atom in the composition.
    Finally gets the data from materials project.
    Create CSV file if already not there or append
    if exists.
    """
    
    df_2d = read_2d(db_2d_file)
    
    comp_list = composition_list(start_z, end_z, noatoms)
    
    for comp in comp_list:
        
        data_list = get_data(comp, df_2d)
        
        if len(data_list) !=0:
        
            for data in data_list:
                print(data)

                if path.isfile(final_csv):
                    append_csv(final_csv,data)
                else:
                    create_csv(final_csv)
                    append_csv(final_csv,data)
        
    