
Utility functions to manipulate structures and data scraping for future
machine learning. The functions rely on [ASE](https://wiki.fysik.dtu.dk/ase/)
and [pymatgen](http://pymatgen.org/) for structure manipulation and data collection.

## Table of contents

* [Structure manipulation](#markdown-header-structure-manipulation)
	* [Check convergence](#markdown-header-check_convergence)
	* [Binding energy calculation](#markdown-header-calc_binding_energy)
	* [Get distance of adsorption site from a surface](#markdown-header-get_distance)
	* [Get angle between two vectors](#markdown-header-angle_between)
	* [Rotate a vector towards another vector](#markdown-header-rotate_towards)
	* [Surface from 2H layered bulk](#markdown-header-bulk_to_nlayer)
	* [Set initial magnetic moment for ABX3 type material](#markdown-header-mn_initial_moment)
	* [Build heterostructure](#markdown-header-aa_ab)
	* [Build bulk 2H layered](#markdown-header-make2h)
* [Data scraping for maching learning](#markdown-header-data-scraping)
	* [Create CSV file](#markdown-header-create_csv)
	* [Append to CSV file](#markdown-header-append_csv)
	* [Create list of compositions from symbols](#markdown-header-composition_list)
	* [Matches a composition with 2D dataframe](#markdown-header-match_2d)
	* [Get data for a composition from Materials Project](#markdown-header-get_data)
	* [Use all functions to consolidate data](#markdown-header-do_all)

Structure Manipulation
======================

check_convergence
--------------------

Checks convergence from the vasp output file.
    
Constructs file path from the given input parameters
and checks the last line to check if the calculation
is converged or not.


calc_binding_energy
--------------------

Calculates binding energy of a B/N atom on 111 surface.

This function is used to calculate binding energy of a
B/N atom on a (111) supercell surface. The function
reads the energies from a text file inside a directory
structure and returns
binding energy of the B/N atom normalized by the
supercell size.

The code assumes the directory structure as:
`supercell size -> boron/nitrogen -> data file`

The structure of text file with the energies are:
```
Config     Energy (eV)
 hcp       -198.4350
 ni-surf   -192.3473
 fcc       -197.9562
 ontop     -196.5833
```

get_distance
--------------------

Calculates distance of adsorption site from a surface.

This function can be used to calculate distance of an
adsorption atom/layer from a surface. The function
reads the structure from a directory structure
and returns the distance.

The returned value is a dictionary with the sites as key.

unit_vector
--------------------

Returns the unit vector of a given vector.

angle_between
--------------------

Returns the angle in radians between vectors 'v1' and 'v2':

```python
>>> angle_between((1, 0, 0), (0, 1, 0))
1.5707963267948966
>>> angle_between((1, 0, 0), (1, 0, 0))
0.0
>>> angle_between((1, 0, 0), (-1, 0, 0))
3.141592653589793
```

rotate_towards
--------------------

Rotates one vector v1 towards v2 by angle theta.
    
From discussion in [Stack Overflow](https://stackoverflow.com/a/22101541).


bulk_to_nlayer
---------------

Takes a 2H bulk 2D material and returns a surface with n-layers.

Mn_initial_moment
----------------

This function initialized magnetic moments for MnBX3 AFM materials.

MnBX3 are Neel type AFM materials. In the same layer the ordering of the spin
are alternating.
Also the two Mn atoms of two layer in same position have alternating ordering.
The `def_mom` is the default magnetic moment for atoms other than Mn.
The `mag_mom` is the magnetic moment for Mn atoms.
If `mag_mom` have a single value, then the values are assigned for collinear calculations.
If `mag_mom` is a list of three values, then the values are assigned for non-collinear calculations.

aa_ab
------------------

Creates a heterostructure from the given materials.
    
Given the bottom and top structure,
the function creates a heterostructure surface.
The top layer is strained to the bottom layer.
The default stacking is AA.
Other stacking possible are AB and AC.
Stacking is based on the position of 
metal atom.
Default vacuum for the surface is 7.5*2=15 angstroms.


make2h
-----------------

Creates a bulk 2H structure from the input parameters.
    
Example `formula` is MoS2, `a` is the lattice constanct,
`thick` is the thickness of the layer and `vac` is
the vacuum on each side of the layer.

Data scraping
=============

create_csv
----------

Function to create csv file.
    
This function creates a csv file with the column
names mentioned in the fieldnames variable.

append_csv
----------

Appends to the created CSV file.
    
Open CSV file in append mode and
append row to the end.

composition_list
----------------

Creates list of compositions
    
This function creates a list of composition from atomic number
`start_z` to `end_z`. Then returns a permutation of composition
as a list.

Optionally avoids elements supplied in radioactive list. Default
elements in the radioactive list are Tc and Pm.

match_2d
---------

This function matches whether the material is a layered 2D material or not
    
This function takes pretty formula, spacegroup and dataframe with
information about layered 2D materials
as input and returns 1 if material is 2D and 0 otherwise.

get_data
---------

Gets data from Materials Project using the MPRester API.

Creates a data row from the given composition using the MPRester API of pymatgen
for a binary composition. Can be extended to other systems too.
Returns a row which can be appended using the append_csv function.

do_all
------

Combines all the function for data collection.

Opens 2D database and put that into a dataframe.
Then creates a list of composition based on range of atomic
number and number of atom in the composition.
Finally gets the data from materials project.
Create CSV file if already not there or append
if exists.

